//
//  ErrorView.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by Prashanth on 6/6/22.
//

import SwiftUI

struct ErrorView: View {
    @ObservedObject var allSchools: AllSchoolsAdapter
    var body: some View {
        VStack {
            
            Text("Some error icon")
                .font(.system(size: 80))
            
            Text(allSchools.errorMessage ?? "")
            
            Button {
                allSchools.fetchAllSchools()
            } label: {
                Text("Try again")
            }

            
        }
    }
}

struct ErrorView_Previews: PreviewProvider {
    static var previews: some View {
        ErrorView(allSchools: AllSchoolsAdapter())
    }
}
