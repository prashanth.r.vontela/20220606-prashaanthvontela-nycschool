//
//  ContentView.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by Prashanth on 6/6/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject var schoolFetcher = AllSchoolsAdapter()
    
    var body: some View {
        if schoolFetcher.isLoading {
            LoadingView()
        } else if schoolFetcher.errorMessage != nil {
            ErrorView(allSchools: schoolFetcher)
        } else {
            SchoolListView(schools: schoolFetcher.schools)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
