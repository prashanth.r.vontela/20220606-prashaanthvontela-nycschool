//
//  GetSchoolList.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by Prashanth on 6/6/22.
//

import Foundation

class SchoolFetcher: ObservableObject {
    
    @Published var schools = [School]()
    @Published var isLoading: Bool = false
    @Published var errorMessage: String? = nil
    
    let service: APIServiceProtocol
    
    init(service: APIServiceProtocol  = APIService()){
        self.service = service
        fetchAllSchools()
    }
    
    func fetchAllSchools () {
        isLoading = true
        errorMessage = nil
        let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
        service.fetchSchools(url: url, completion: { [unowned self] result in
            DispatchQueue.main.async {
                self.isLoading = false
                switch result {
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                case .success(let schools):
                    self.schools = schools
                }
            }
            
        })
        
    }
    
    
    
    //MARK: preview helpers
    
    static func errorState() -> SchoolFetcher {
        let fetcher = SchoolFetcher()
        fetcher.errorMessage = APIError.url(URLError.init(.notConnectedToInternet)).localizedDescription
        return fetcher
    }
    
    static func successState() -> SchoolFetcher {
        let fetcher = SchoolFetcher()
        fetcher.schools = [School.example1(), School.example2()]
        return fetcher
    }
    
}
