//
//  School.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by Prashanth on 6/6/22.
//

import Foundation


/*
 "dbn": "01M292",
"school_name": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
"num_of_sat_test_takers": "29",
"sat_critical_reading_avg_score": "355",
"sat_math_avg_score": "404",
"sat_writing_avg_score": "363"
 */

struct School: Codable, CustomStringConvertible, Identifiable {
    let id: String
    let name: String
    let testTakers: String
    let readingAvgScore: String
    let mathAvgScore: String
    let writingAvgScore: String
    
    var description: String {
        "School with name: \(name) and id : \(id)"
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "dbn"
        case name = "school_name"
        case testTakers = "num_of_sat_test_takers"
        case readingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        testTakers = try values.decode(String.self, forKey: .testTakers)
        mathAvgScore = try values.decode(String.self, forKey: .mathAvgScore)
        readingAvgScore = try values.decode(String.self, forKey: .readingAvgScore)
        writingAvgScore = try values.decode(String.self, forKey: .writingAvgScore)
    }
    
    
    init(id: String, name: String, testTakers: String, readingAvgScore: String, mathAvgScore: String, writingAvgScore: String) {
        self.id = id
        self.name = name
        self.testTakers = testTakers
        self.mathAvgScore = mathAvgScore
        self.readingAvgScore = readingAvgScore
        self.writingAvgScore = writingAvgScore
    }
    
    
    static func example1() -> School {
        return School(
        id: "01M292", name: "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES", testTakers: "29", readingAvgScore: "355", mathAvgScore: "404", writingAvgScore: "363"
        )
    }
    
    static func example2() -> School {
        return School(
        id: "01M448", name: "UNIVERSITY NEIGHBORHOOD HIGH SCHOOL", testTakers: "91", readingAvgScore: "383", mathAvgScore: "423", writingAvgScore: "366"
        )
    }
}
