//
//  SchoolListView.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by Prashanth on 6/6/22.
//

import SwiftUI

struct SchoolListView: View {
    let schools: [School]
    
    @State private var searchText: String = ""
    
    var filteredSchools: [School] {
        if searchText.count == 0 {
            return schools
        } else {
            return schools.filter { $0.name.lowercased().contains(searchText.lowercased())
            }
        }
    }
    
    var body: some View {
        NavigationView {
            if #available(iOS 15.0, *) {
                List {
                    ForEach(filteredSchools) { school in
                        NavigationLink {
                            SchoolDetailView(schoolId: school.id)
                        } label: {
                            SchoolRow(school: school)
                        }
                        
                    }
                }
                .listStyle(PlainListStyle())
                .navigationTitle("Find your school")
                .searchable(text: $searchText)
            } else {
                // Fallback on earlier versions
            }
        }
    }
}

struct SchoolListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolListView(schools: AllSchoolsAdapter.successState().schools)
    }
}
