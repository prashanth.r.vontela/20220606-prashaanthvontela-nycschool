//
//  SchoolRow.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by lpalle on 6/6/22.
//

import SwiftUI

struct SchoolRow: View {
    let school: School
    
    var body: some View {
        VStack {
            Text(school.name)
            HStack {
                Text("Test Taker:" + school.testTakers)
                Text("Math Score: " + school.mathAvgScore)
            }
            
            HStack {
                Text("Writing Score: \(school.writingAvgScore)")
                Text("Reading Score: \(school.readingAvgScore)")
            }
            
        }
    }
}

struct SchoolRow_Previews: PreviewProvider {
    static var previews: some View {
        SchoolRow(school: School.example1())
    }
}
