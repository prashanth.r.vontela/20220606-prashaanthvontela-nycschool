//
//  SchoolDetail.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by Prashanth on 6/7/22.
//

import Foundation


struct SchoolDetail: Codable, CustomStringConvertible {
    let name: String
    let overview: String
    let phone: String
    let email: String
    let location: String
    let website: String
    
    var description: String {
        ""
    }
    
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case overview = "overview_paragraph"
        case phone = "phone_number"
        case email = "school_email"
        case location = "location"
        case website = "website"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        overview = try values.decode(String.self, forKey: .overview)
        phone = try values.decode(String.self, forKey: .phone)
        email = try values.decode(String.self, forKey: .email)
        location = try values.decode(String.self, forKey: .location)
        website = try values.decode(String.self, forKey: .website)
        
    }
}
