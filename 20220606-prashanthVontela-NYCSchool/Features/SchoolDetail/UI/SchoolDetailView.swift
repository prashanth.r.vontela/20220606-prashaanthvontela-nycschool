//
//  SchoolDetailView.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by Prashanth on 6/6/22.
//

import SwiftUI

struct SchoolDetailView: View {
    let schoolId: String
    
    @StateObject var schoolDetailAdapter = SchoolDetailAdapter()
    
    
    var body: some View {
        Text(schoolDetailAdapter.schoolDetail?.name ?? "NO NAME").onAppear(perform: {
            schoolDetailAdapter.fetchSchoolDetail(id: schoolId)
        })
    }
}

//struct SchoolDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        SchoolDetailView()
//    }
//}
