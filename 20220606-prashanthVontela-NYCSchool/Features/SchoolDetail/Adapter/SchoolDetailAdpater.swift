//
//  SchoolDetailAdpater.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by lpalle on 6/7/22.
//

import Foundation

class SchoolDetailAdapter: ObservableObject {
   
    @Published var schoolDetail: SchoolDetail? = nil
    @Published var isLoading: Bool = false
    @Published var errorMessage: String? = nil
    
    
    let service: APIServiceProtocol
    
    init(service: APIServiceProtocol = APIService()) {
        self.service = service
//        fetchSchoolDetail(id: id)
    }
    
    func fetchSchoolDetail(id: String) {
        isLoading = true
        errorMessage = nil
        var myUrl = URLComponents(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        var items = [URLQueryItem]()
        items.append(URLQueryItem(name: "dbn", value: id))
        myUrl?.queryItems = items
        let request = URLRequest(url: myUrl!.url!)
        service.fetchSchoolDetail(urlRequest: request, completion: { [unowned self] result in
            DispatchQueue.main.async {
                self.isLoading = false
                switch result {
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                case .success(let schoolDetail):
                    self.schoolDetail = schoolDetail
                }
            }
            
        })
        
    }
}
