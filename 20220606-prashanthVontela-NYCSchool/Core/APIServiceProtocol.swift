//
//  APIServiceProtocol.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by Prashanth on 6/6/22.
//

import Foundation

protocol APIServiceProtocol {
    func fetchSchools(url: URL?, completion:@escaping(Result<[School], APIError>) -> Void)
    func fetchSchoolDetail(urlRequest: URLRequest?, completion:@escaping(Result<SchoolDetail, APIError>) -> Void)
}
