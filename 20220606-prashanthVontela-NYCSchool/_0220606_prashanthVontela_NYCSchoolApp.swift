//
//  _0220606_prashanthVontela_NYCSchoolApp.swift
//  20220606-prashanthVontela-NYCSchool
//
//  Created by Prashanth on 6/6/22.
//

import SwiftUI

@main
struct _0220606_prashanthVontela_NYCSchoolApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
